from django.urls import path
from shopping_list import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('create/', views.ProductCreateView.as_view(), name='create'),
    path('products/<int:pk>/', views.ProductDetailView.as_view(), name='detail'),
    path('edit/<int:pk>/', views.ProductUpdateView.as_view(), name='edit'),
    path('delete/<int:pk>/', views.ProductDeleteView.as_view(), name='delete')
]
