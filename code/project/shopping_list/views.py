from django.shortcuts import render, redirect, get_object_or_404
from shopping_list.models import Product
from shopping_list.forms import ProductForm
from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView


class IndexView(ListView):
    template_name = 'index.html'
    context_object_name = 'products'

    def get_queryset(self):
        return Product.objects.all()


class ProductCreateView(CreateView):
    model = Product
    form_class = ProductForm
    template_name = 'create.html'
    success_url = '/'


class ProductDetailView(DetailView):
    model = Product
    template_name = 'detail.html'


class ProductUpdateView(UpdateView):
    model = Product
    template_name = 'edit.html'
    form_class = ProductForm
    success_url = '/'


class ProductDeleteView(DeleteView):
    model = Product
    template_name = 'delete.html'
    success_url = '/'
