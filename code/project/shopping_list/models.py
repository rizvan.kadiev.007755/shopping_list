from django.db import models


class Product(models.Model):
    name = models.CharField(verbose_name="Название", max_length=128)
    price = models.PositiveIntegerField(verbose_name="Цена")
    description = models.TextField(verbose_name="Описание", max_length=1000)
    create_date = models.DateTimeField(verbose_name="Время создания", auto_now_add=True)
    update_date = models.DateTimeField(verbose_name="Время изменения", auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"
