from django.contrib import admin
from shopping_list.models import Product

admin.site.register(Product)
